<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PostsController extends Controller
{
    /**
     * @Route("/", name="posts-index")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');

        $posts = $repository->findAll();

        $form = $this->createForm(PostType::class, null, [
            'action' => $this->generateUrl('post-create'),
        ]);

        return $this->render('posts/index.html.twig', ['posts' => $posts, 'form' => $form->createView()]);
    }

    /**
     * @Route("/posts/create", name="post-create")
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(PostType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $post = new Post();
            $post->setTitle($data['title']);
            $post->setBody($data['body']);
            //TODO: set these timestamps automatically from the entity
            $post->setCreatedAt( new \DateTime() );
            $post->setModifiedAt( new \DateTime() );

            $em = $this->getDoctrine()->getManager();

            $em->persist($post);

            $em->flush();
        }

        return $this->redirectToRoute('posts-index');
    }

    /**
     * @Route("/posts/{id}", name="post-view")
     */
    public function readAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);
        if (!$post) {
            throw $this->createNotFoundException(
                'No post found for id '.$id
            );
        }

        $form = $this->createForm(PostType::class, $post, [
            'action' => $this->generateUrl('post-update', ['id' => $id]),
        ]);

        return $this->render('posts/view.html.twig', ['post' => $post, 'form' => $form->createView()]);
    }

    /**
     * @Route("/posts/{id}/update", name="post-update")
     */
    public function updateAction(Request $request, $id)
    {
        $form = $this->createForm(PostType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);
            if (!$post) {
                throw $this->createNotFoundException(
                    'No post found for id '.$id
                );
            }
            $post->setTitle($data['title']);
            $post->setBody($data['body']);
            //TODO: set this timestamp automatically from the entity
            $post->setModifiedAt( new \DateTime() );

            $em = $this->getDoctrine()->getManager();

            $em->persist($post);

            $em->flush();
        }

        return $this->redirectToRoute('post-view', ['id' => $id]);
    }

    /**
     * @Route("/posts/{id}/delete", name="post-delete")
     */
    public function deleteAction(Request $request, $id)
    {
        if(!$request->isMethod('post')) {
            throw $this->createNotFoundException(
                'Only accessable with post requests'
            );
        }

        $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);
        if (!$post) {
            throw $this->createNotFoundException(
                'No post found for id '.$id
            );
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($post);
        $em->flush();

        return $this->redirectToRoute('posts-index');
    }
}